package com.example.mampu.poppinonot.retrofit;

import com.example.mampu.poppinonot.models.EventsModel;
import com.example.mampu.poppinonot.models.UserModel;
import com.example.mampu.poppinonot.models.loginResponseModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface PoppinService {

    @POST("user/login")
    Call<loginResponseModel> login(@Body UserModel userModel);

    @POST("user/register")
    Call<UserModel> signUp(@Body UserModel userModel);

    @GET("event")
    Call<List<EventsModel>> getEvents();

}
