package com.example.mampu.poppinonot;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;

public class Splash extends AppCompatActivity {

    Button btnExplore;
    LinearLayout layPop,layOr,layNot,layBtn;
    Animation upToDown,downToUp,leftToRight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        btnExplore = findViewById(R.id.btnExplore);
        layPop = findViewById(R.id.layoutPoppin);
        layOr = findViewById(R.id.layoutOr);
        layNot = findViewById(R.id.layoutNot);
        layBtn = findViewById(R.id.layoutBtn);
        upToDown = AnimationUtils.loadAnimation(this,R.anim.uptodown);
        downToUp = AnimationUtils.loadAnimation(this,R.anim.downtoup);
        leftToRight = AnimationUtils.loadAnimation(this,R.anim.left_to_right);

        layPop.setAnimation(downToUp);
        layBtn.setAnimation(leftToRight);
        layOr.setAnimation(downToUp);
        layNot.setAnimation(upToDown);

        btnExplore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Splash.this, Home.class);
                startActivity(intent);
            }
        });


    }


}
