package com.example.mampu.poppinonot.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserModel {

    @SerializedName("user_ID")
    @Expose
    public int user_ID;
    @SerializedName("name")
    @Expose
    public String Name;
    @SerializedName("surname")
    @Expose
    public String Surname;
    @SerializedName("gender")
    @Expose
    public String Gender;
    @SerializedName("cellnumber")
    @Expose
    public String Cellnumber;
    @SerializedName("email")
    @Expose
    public String Email;
    @SerializedName("pass")
    @Expose
    public String Pass;

    /**
     * Constructor used for passing signUp information to webAPI
     * @param name
     * @param surname
     * @param gender
     * @param cellnumber
     * @param email
     * @param pass
     */
    public UserModel(String name,String surname,String gender,String cellnumber,String email,String pass){
        Name = name;
        Surname = surname;
        Gender = gender;
        Cellnumber = cellnumber;
        Email = email;
        Pass = pass;
    }

    /**
     * Constructor used for passing Login information to webAPI
      * @param email
     * @param pass
     */
    public UserModel(String email,String pass){
        Email = email;
        Pass = pass;
    }

    public int getUser_ID() {
        return user_ID;
    }

    public void setUser_ID(int user_ID) {
        this.user_ID = user_ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String surname) {
        Surname = surname;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getCellnumber() {
        return Cellnumber;
    }

    public void setCellNumber(String cellnumber) {
        Cellnumber = cellnumber;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPass() {
        return Pass;
    }

    public void setPass(String pass) {
        Pass = pass;
    }

}
