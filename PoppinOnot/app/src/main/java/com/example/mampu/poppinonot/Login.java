package com.example.mampu.poppinonot;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mampu.poppinonot.models.UserModel;
import com.example.mampu.poppinonot.models.loginResponseModel;
import com.example.mampu.poppinonot.retrofit.RetrofitService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {

    SharedPreferences pref;
    EditText email,pass;
    Button signIn;
    TextInputLayout tilEmail,tilPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email = findViewById(R.id.txtEmail);
        pass = findViewById(R.id.txtPassword);
        tilEmail = findViewById(R.id.tilEmail);
        tilPass = findViewById(R.id.tilPass);
        signIn = findViewById(R.id.btnLogin);

        signIn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
               if (validateEmail(email)&& validatePass(pass)) {
                    signInPoint();
                }
            }
        });
    }

    private boolean validatePass(EditText editText) {
        if (editText.getText().toString().trim().length() > 0) {
            tilEmail.setError(null);
            return true; // returns true if field is not empty
        }
        tilPass.setError("Please fill field");
        return false;
    }

    private boolean validateEmail(EditText editText) {
        if (editText.getText().toString().trim().length() > 0) {
            tilEmail.setError(null);
            return true; // returns true if field is not empty
        }
        tilEmail.setError("Please fill field");
        return false;
    }

    public void signInPoint(){
        final ProgressDialog progressDialog = new ProgressDialog(Login.this);
        progressDialog.setCancelable(false); // set cancelable to false
        progressDialog.setMessage("Logging On ..."); // set message
        progressDialog.show(); // show progress dialog

        String email1 = email.getText().toString().trim();
        String pass1 = pass.getText().toString().trim();

        UserModel userModel = new UserModel(email1,pass1);

        (RetrofitService.getClient().login(userModel)).enqueue(new Callback<loginResponseModel>() {
            @Override
            public void onResponse(Call<loginResponseModel> call, Response<loginResponseModel> response) {
                pref  = getApplicationContext().getSharedPreferences("MyPref", 0);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("token",response.body().Token);
                editor.commit();

                Intent intent = new Intent(Login.this, Home.class);
                startActivity(intent);
                progressDialog.dismiss();
            }
            @Override
            public void onFailure(Call<loginResponseModel> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"Invalid User Details", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
}
