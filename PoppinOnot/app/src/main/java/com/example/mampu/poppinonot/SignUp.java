package com.example.mampu.poppinonot;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mampu.poppinonot.models.UserModel;
import com.example.mampu.poppinonot.retrofit.PoppinService;
import com.example.mampu.poppinonot.retrofit.RetrofitService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUp extends AppCompatActivity {

    UserModel UserModelData;
    EditText name,surname,gender,cellnumber,email,pass,confirmPass;
    Button signUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        name = findViewById(R.id.txtNameR);
        surname = findViewById(R.id.txtSurnameR);
        gender = findViewById(R.id.txtGenderR);
        cellnumber = findViewById(R.id.txtMobileNoR);
        email = findViewById(R.id.txtEmailR);
        pass = findViewById(R.id.txtPasswordR);
        confirmPass = findViewById(R.id.txtConPasswordR);
        signUp = findViewById(R.id.btnSignUp);

        signUp.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                if (validate(name) && validate(pass)) {
                    signUpPoint();
                }
            }
        });
    }

    private boolean validate(EditText editText) {
        // check the length of the enter data in EditText and give error if its empty
        if (editText.getText().toString().trim().length() > 0) {
            return true; // returns true if field is not empty
        }
        editText.setError("Please Fill This");
        editText.requestFocus();
        return false;
    }

    private void signUpPoint() {
        // display a progress dialog
        final ProgressDialog progressDialog = new ProgressDialog(SignUp.this);
        progressDialog.setCancelable(false); // set cancelable to false
        progressDialog.setMessage("Creating Account..."); // set message
        progressDialog.show(); // show progress dialog

        //getting values from text fields
        String name1 = name.getText().toString().trim();
        String surname1 = surname.getText().toString().trim();
        String gender1 = gender.getText().toString().trim();
        String cellNumber1 = cellnumber.getText().toString().trim();
        String email1 = email.getText().toString().trim();
        String pass1 = pass.getText().toString().trim();
        String pass2  = confirmPass.getText().toString().trim();

        if(pass1.equals(pass2)) {
            UserModel userModel = new UserModel(name1, surname1, gender1, cellNumber1, email1, pass1);

            // Retrofit is a class in which we define a method getClient() that returns the API Interface class object
            // registration is a POST request type method in which we are sending our field's data
            // enqueue is used for callback response and error
            (RetrofitService.getClient().signUp(userModel)).enqueue(new Callback<UserModel>() {
                @Override
                public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                    UserModelData = response.body();
                    // Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(SignUp.this, Home.class);
                    startActivity(intent);
                    progressDialog.dismiss();
                }
                @Override
                public void onFailure(Call<UserModel> call, Throwable t) {
                    Log.d("response", t.getStackTrace().toString());
                    progressDialog.dismiss();
                }
            });
        }else{
            Toast.makeText(getApplicationContext(),"Password Mismatch",Toast.LENGTH_SHORT).show();
            progressDialog.dismiss();
        }
    }
}
