package com.example.mampu.poppinonot.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.mampu.poppinonot.R;
import com.example.mampu.poppinonot.models.EventsModel;

import java.util.List;

public class EventAdapter  extends RecyclerView.Adapter<EventAdapter.EventViewHolder> {

    Context context;
    List<EventsModel> eventsModelListData;

    public EventAdapter(Context context,List<EventsModel> eventsModelListData){
        this.eventsModelListData = eventsModelListData;
        this.context = context;
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item,null);
        EventViewHolder eventViewHolder = new EventViewHolder(view);
        return eventViewHolder;
    }

    @Override
    public void onBindViewHolder(EventViewHolder holder, int position) {
        holder.name.setText(eventsModelListData.get(position).getName());
        holder.place.setText(eventsModelListData.get(position).getPlace());
        holder.date.setText(eventsModelListData.get(position).getDate());

        String image_url = eventsModelListData.get(position).getImage_url();
        Glide.with(context)
                .load(image_url)
                .into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return eventsModelListData.size();
    }


    class EventViewHolder extends RecyclerView.ViewHolder{
        TextView place,name,date;
        ImageView imageView;

        public EventViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            place = itemView.findViewById(R.id.place);
            date = itemView.findViewById(R.id.date);
            imageView = itemView.findViewById(R.id.full_image);
        }
    }
}
