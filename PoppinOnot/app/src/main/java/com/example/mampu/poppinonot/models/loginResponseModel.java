package com.example.mampu.poppinonot.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * This Model is used for validating the response code
 * the comes back from the api wether its a error code or
 */
public class loginResponseModel {

    @SerializedName("id")
    @Expose
    public int user_ID;
    @SerializedName("name")
    @Expose
    public String Name;
    @SerializedName("gender")
    @Expose
    public String Gender;
    @SerializedName("contact")
    @Expose
    public String Cellnumber;
    @SerializedName("email")
    @Expose
    public String Email;
    @SerializedName("token")
    @Expose
    public String Token;

    public int getUser_ID() {
        return user_ID;
    }

    public void setUser_ID(int user_ID) {
        this.user_ID = user_ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getCellnumber() {
        return Cellnumber;
    }

    public void setCellnumber(String cellnumber) {
        Cellnumber = cellnumber;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

}
