package com.example.mampu.poppinonot.models;

import android.icu.text.DateFormat;
import android.icu.text.SimpleDateFormat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.util.Date;

public class EventsModel {


    @SerializedName("event_ID")
    @Expose
    private int event_ID;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("likes")
    @Expose
    private int likes;
    @SerializedName("disLikes")
    @Expose
    private int disLikes;
    @SerializedName("place")
    @Expose
    private String place;
    @SerializedName("passcard_ID")
    @Expose
    private int passcard_ID;
    @SerializedName("image_Url")
    @Expose
    private String image_Url;

    public int getEvent_id() {
        return event_ID;
    }

    public void setEvent_id(int event_ID) {
        this.event_ID = event_ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            DateFormat converter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"); //Date format string is passed as an argument to the Date format object
            DateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy");
            Date date1 ;
            String date2 = null;
            try{
                date1 = converter.parse(date);
                date2  = formatter.format(date1);

           }catch (ParseException e){

           }
            return date2;
        }

        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public int getDislikes() {
        return disLikes;
    }

    public void setDislikes(int dislike) {
        this.disLikes = dislike;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public int getPasscard_id() {
        return passcard_ID;
    }

    public void setPasscard_id(int passcard_ID) {
        this.passcard_ID = passcard_ID;
    }

    public String getImage_url() {
        return image_Url;
    }

    public void setImage_url(String image_Url) {
        this.image_Url = image_Url;
    }



}
