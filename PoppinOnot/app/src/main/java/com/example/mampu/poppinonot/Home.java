package com.example.mampu.poppinonot;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.mampu.poppinonot.adapters.EventAdapter;
import com.example.mampu.poppinonot.models.EventsModel;
import com.example.mampu.poppinonot.retrofit.RetrofitService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Home extends AppCompatActivity {

    RecyclerView recyclerView;
    List<EventsModel> eventsModelData;
    DrawerLayout dLayout;
    ImageButton imageButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        SharedPreferences pref  = getApplicationContext().getSharedPreferences("MyPref", 0);
        String x =  pref.getString("token",null);
        Toast.makeText(Home.this ,x, Toast.LENGTH_SHORT).show();
        //Log.e("TOKEN$$$$___",pref.getString("token",null));


        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        imageButton = findViewById(R.id.menu_image_button);
        //side bar
        setNavigationDrawer();
        //on menu item click
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dLayout.openDrawer(GravityCompat.START);
            }
        });

        recyclerView = findViewById(R.id.recyclerView);
        getEventListData();
    }

    /**
     * functions that gets events from Api and fills them into an
     * recyclerView
     */
    private void getEventListData(){

        //loading to show user something is happening
       final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Fetching Events... Please wait");
        progressDialog.show();

       (RetrofitService.getClient().getEvents()).enqueue(new Callback<List<EventsModel>>(){
            @Override
            public void onResponse(Call<List<EventsModel>> call, Response<List<EventsModel>> response) {
                progressDialog.dismiss();
                eventsModelData = response.body();
                setDataInRecyclerView();
            }

            @Override
            public void onFailure(Call<List<EventsModel>> call, Throwable t) {
                Toast.makeText(Home.this,"Error",Toast.LENGTH_LONG).show();
                Log.e("ERROR","FAIL TO PULL DATA");
                progressDialog.dismiss();
            }
        });

    }

    /**
     *Function that set data from retrofit onto the recyclerView
     */
    private void setDataInRecyclerView(){
        //setting a linear layout with default vertical orientation
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);//add layout to the recyclerView
        //Binding stuff in the eventAdapter

        EventAdapter eventAdapter = new EventAdapter(this, eventsModelData);
        recyclerView.setAdapter(eventAdapter);
    }

    /**
     * Function contains the side navigation drawer
     */
    private void setNavigationDrawer() {
        dLayout =  findViewById(R.id.drawer_layout); // initiate a DrawerLayout
        NavigationView navView = findViewById(R.id.navigation); // initiate a Navigation View

        // implement setNavigationItemSelectedListener event on NavigationView
        navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                Intent intent;
                int itemId = menuItem.getItemId(); // get selected menu item's id
                // check selected menu item's id and replace a Fragment Accordingly
                if (itemId == R.id.first) {
                    intent = new Intent(Home.this, Home.class);
                    startActivity(intent);
                } else if (itemId == R.id.third) {
                    intent = new Intent(Home.this, SignUp.class);
                    startActivity(intent);
                } else if (itemId == R.id.fourth) {
                    intent = new Intent(Home.this, Login.class);
                    startActivity(intent);
                }else if(itemId == R.id.fifth){
                    Toast.makeText(Home.this,"Coming Soon",Toast.LENGTH_LONG).show();
                }
                else if(itemId == R.id.second){
                    Toast.makeText(Home.this,"Coming Soon",Toast.LENGTH_LONG).show();
                }
                return false;
            }
        });
    }
}
