package com.example.mampu.poppinonot.retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitService {
    private static Retrofit retrofit = null;

    public static PoppinService getClient() {

        // change your base URL
        if(retrofit == null) {
            retrofit = new Retrofit.Builder()
                    //.baseUrl("http://10.0.2.2:52019/api/")
                    .baseUrl("http://www.operationjumperstart.co.za/api/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        //Creating object for our interface
        PoppinService api = retrofit.create(PoppinService.class);
        return api; // return the APIInterface object
    }
}
